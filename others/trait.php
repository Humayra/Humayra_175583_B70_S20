<?php

    trait A{

        public function tellA(){
            echo "I'm A<br>";
        }

    }

    trait B{

        public function tellB(){
            echo "I'm B<br>";
        }
    }

    class C{

        use A,B;
    }

    $objC = new C();
    $objC->tellA();